#from user import User
#from database import Database
#from twitter_utils import get_request_token, get_oauth_verifier, get_access_token

#Database.initialise(user='postgres', password="Hellfish3", host='localhost', database='learning')

import constants
import oauth2
import urllib.parse as urlparse

consumer = oauth2.Consumer(constants.CONSUMER_KEY, constants.CONSUMER_SECRET)
client = oauth2.Client(consumer)

response, content = client.request(constants.REQUEST_TOKEN_URL, 'POST')
if response.status != 200:
   print("An error occured getting the request the request token from Twitter")

request_token = dict(urlparse.parse_qsl(content.decode('utf-8')))


print("Go to the following website in your browser")
print("{}?oauth_token={}".format(constants.AUTHORIZATION_URL,request_token['oauth_token']))
#print(request_token)

oauth_verifier =input('What is your pin..?')
